#include "Tree.h"

unsigned int Tree::GetSize(const Tree* t) const
{
    if(!t) 
        return 0;
    if(!t->left && !t->right)
        return 1;
    unsigned int left = 0, right = 0;

    if(t->left)
        left = GetSize(t->left);
    if(t->right)
        right = GetSize(t->right);
    
    return left + right + 1;
}

void Tree::Add(Tree*& t, int data)
{
    if(!t)
    {
        t = new Tree;
        t->data = data;
        t->left = 0;
        t->right = 0;
        return;
    }
    if(data < t->data)
        Add(t->left, data);
    else  
        Add(t->right, data);
}

Tree* Tree::Search(const Tree*& t, int data) const
{
    if(!t) 
        return 0;
    while(t->data != data)
    {
        if(data < t->data)
            t = t->left;
        else
            t = t->right;
        if(!t)
            break;
    }

    return t;
}

bool Tree::Delete(Tree*& t, std::ostream* out)
{
    if(t)
    {
        if(t->left)
            Delete(t->left);
        if(t->right)
            Delete(t->right);
        
        delete t;
        t = 0;

        return true;
    }
    else
    {
        if(out)
            *out << "Tree is empty!\n";

        return false;
    }
}

