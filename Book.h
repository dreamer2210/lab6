#pragma once
#include <string>
#include <ostream>
#include <istream>

struct Book
{
	unsigned int pages;
	double price;
	std::string name;
	std::string author;

	Book& operator = (const Book& obj);

	friend std::ostream& operator << (std::ostream& stream, const Book& obj);
	friend std::istream& operator >> (std::istream& stream, Book& obj);
};