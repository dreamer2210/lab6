#pragma once
#include <ostream>

struct Tree
{
    int data;
    Tree* left;
    Tree* right;

    unsigned int GetSize(const Tree* t) const;
    void Add(Tree*& t, int data);
    Tree* Search(const Tree*& t, int data) const;
    bool Delete(Tree*& t, std::ostream* error = 0);
};
